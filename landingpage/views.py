from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm
from datetime import datetime

# Create your views here.
def index(request):
    response = {
        'form' : StatusForm,
        'status' : Status.objects.all(),
    }

    return render(request, 'index.html', response)

def add_status(request):
    form = StatusForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        status = Status.objects.create(status=request.POST['status'])
    
    return redirect('landingpage:index')
