from django import forms

class StatusForm(forms.Form):
    status = forms.CharField(
        label='The Lazy Status',
        label_suffix=' = '
        )