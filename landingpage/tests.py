from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.utils import timezone

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from .models import Status
from .forms import StatusForm
from . import views

# Create your tests here.
class LandingPageTest(TestCase):

    def test_landingpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landingpage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(date=timezone.now(), status='Jombs forevah!')
        
        counting_all_avalaible_status = Status.objects.all().count()
        self.assertEqual(counting_all_avalaible_status, 1)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/add_status',
                    data={
                        'date' : '2019-10-28T00:00',
                        'status' : 'Jombs forevah!'
                    })
        counting_all_avalaible_status = Status.objects.all().count()
        self.assertEqual(counting_all_avalaible_status, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Jombs forevah!', html_response)

    def test_model_is_saved_in_admin_site(self):
        new_status = Status.objects.create(date=timezone.now(), status='Jombs forevah!')
        self.assertEqual(str(new_status), new_status.status)

class LandingPageFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):    
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # for local testing
        # self.selenium = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()

    def test_submit_a_status(self):
        self.selenium.get('%s' % (self.live_server_url))
        input_box = self.selenium.find_element_by_id('id_status')
        input_box.send_keys('Lohe lohe?!')
        input_box.send_keys(Keys.RETURN)
        self.assertIn("Lohe lohe?!", self.selenium.page_source)
